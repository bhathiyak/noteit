package com.example.bhathiyakumarasingha.noteit;

import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


public class AddNoteActivity extends AppCompatActivity {
    ClipData.Item item;
    ImageView imageView;
    private ImageView imageNote;
    EditText title, content;
    private ArrayList<Note> noteList;

    public static DBAdapter dbAdapter;

    String Title;
    String Content;
    int Id = 00000;
    byte[] image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        init();

        dbAdapter = new DBAdapter(this);
        new Bundle();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            Id = b.getInt("id");
            Title = b.getString("title");
            Content = b.getString("content");
            image = b.getByteArray("image");

            title.setText(Title);
            content.setText(Content);
            Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
            imageNote.setImageBitmap(bitmap);

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.note_menu_addnew, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_main_save_image:
                Intent cameraActivity = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraActivity, 10);

                return true;
            case R.id.action_main_save_note:
                Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.default_image);

                if (Id == 00000) {
                    String name = title.getText().toString();
                    if (title.getText().toString().matches("")) {
                        Toast.makeText(getApplicationContext(), "Please Enter a Titile", Toast.LENGTH_SHORT).show();
                    } else if (content.getText().toString().matches("")) {
                        Toast.makeText(getApplicationContext(), "Please Write a Content", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            dbAdapter.insertData(
                                    title.getText().toString().trim(),
                                    content.getText().toString().trim(),
                                    imageViewTobyte(imageNote)
                            );
                            Toast.makeText(getApplicationContext(), "Added successfully!", Toast.LENGTH_SHORT).show();
                            Thread splash_Screen = new Thread() {
                                public void run() {
                                    try {
                                        sleep(250);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        startActivity(new
                                                Intent(getApplicationContext(), MainActivity.class));
                                        finish();
                                    }
                                }
                            };
                            splash_Screen.start();
                        } catch (Exception e) {
                            try {
                                dbAdapter.insertData(
                                        title.getText().toString().trim(),
                                        content.getText().toString().trim(),
                                        BitmapTobyte(b)
                                );
                                Toast.makeText(getApplicationContext(), "Added successfully!", Toast.LENGTH_SHORT).show();
                                Thread splash_Screen = new Thread() {
                                    public void run() {
                                        try {
                                            sleep(250);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        } finally {
                                            startActivity(new
                                                    Intent(getApplicationContext(), MainActivity.class));
                                            finish();
                                        }
                                    }
                                };
                                splash_Screen.start();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                System.out.println("ERROR");
                            }

                        }
                    }
                } else {
                    try {
                        dbAdapter.updateData(
                                title.getText().toString().trim(),
                                content.getText().toString().trim(),
                                imageViewTobyte(imageNote), Id
                        );
                        Toast.makeText(getApplicationContext(), "Updated successfully!", Toast.LENGTH_SHORT).show();
                        Thread splash_Screen = new Thread() {
                            public void run() {
                                try {
                                    sleep(250);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    startActivity(new
                                            Intent(getApplicationContext(), MainActivity.class));
                                    finish();
                                }
                            }
                        };
                        splash_Screen.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("ERROR");

                    }
                }


                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public static byte[] imageViewTobyte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static byte[] BitmapTobyte(Bitmap image) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10 & resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageNote.setImageBitmap(photo);
        }
    }

    public void init() {

        imageNote = (ImageView) findViewById(R.id.imgNoteImage);
        title = (EditText) findViewById(R.id.txtTittle);
        content = (EditText) findViewById(R.id.txtContent);
    }


}
