package com.example.bhathiyakumarasingha.noteit;

public class Note {
    private int id;
    private String name;
    private String content;
    private byte[] image;

    public Note(int id, String name, String content, byte[] image) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
