package com.example.bhathiyakumarasingha.noteit;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

public class DBAdapter extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Note.db";

    public DBAdapter(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void QueryData(String sql) {
        SQLiteDatabase dataBase = getWritableDatabase();
        dataBase.execSQL(sql);


    }

    public void insertData(String Title, String Content, byte[] Image) {
        SQLiteDatabase dataBase = getWritableDatabase();
        String sql = "INSERT INTO NOTES VALUES (null,?,?,?)";

        SQLiteStatement statement = dataBase.compileStatement(sql);
        statement.clearBindings();


        statement.bindString(1, Title);
        statement.bindString(2, Content);
        statement.bindBlob(3, Image);

        statement.executeInsert();
    }

    public void updateData(String title, String content, byte[] image, int Id) {
        SQLiteDatabase database = getWritableDatabase();

        String sql = "UPDATE NOTES SET title = ?, content = ?, image = ? WHERE Id = ?";
        SQLiteStatement statement = database.compileStatement(sql);

        statement.bindString(1, title);
        statement.bindString(2, content);
        statement.bindBlob(3, image);
        statement.bindDouble(4, (double) Id);

        statement.execute();
        database.close();
    }

    public void deleteData(int id) {
        SQLiteDatabase database = getWritableDatabase();

        String sql = "DELETE FROM NOTES WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double) id);

        statement.execute();
        database.close();
    }

    public Cursor searchData(String search) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "SELECT * FROM NOTES WHERE title LIKE " + search + " OR content LIKE " + search;
        Cursor mcursor = database.rawQuery(sql, null);
        return mcursor;
    }

    public Cursor getData(String sql) {
        SQLiteDatabase database = getReadableDatabase();
        Cursor mcursor = database.rawQuery(sql, null);
        return mcursor;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS NOTES(Id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR, content VARCHAR, image BLOB)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS NOTES");
        onCreate(db);
    }

}
