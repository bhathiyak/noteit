package com.example.bhathiyakumarasingha.noteit;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.IdentityHashMap;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.MyViewHolder> {


    private Context context;
    private ArrayList<Note> noteList;
    public DBAdapter dbAdapter;
    RecyclerView recyclerView;


    String Title;
    String Content;
    byte[] image;
    int ID;
    public Note note;

    public NoteAdapter(Context context, ArrayList<Note> noteList) {
        this.context = context;
        this.noteList = noteList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View row;

        LayoutInflater inflater = LayoutInflater.from(context);
        row = inflater.inflate(R.layout.note_list, parent, false);

        return new MyViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.Title.setText(noteList.get(position).getName());
        holder.Content.setText(noteList.get(position).getContent());

        Note note = noteList.get(position);
        final byte[] noteImage = note.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(noteImage, 0, noteImage.length);
        holder.imageView.setImageBitmap(bitmap);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update
                ID = noteList.get(position).getId();
                Title = noteList.get(position).getName();
                Content = noteList.get(position).getContent();
                image = noteList.get(position).getImage();

                Intent intent = new Intent(context, AddNoteActivity.class);
                intent.putExtra("id", ID);
                intent.putExtra("title", Title);
                intent.putExtra("content", Content);
                intent.putExtra("image", image);

                context.startActivity(intent);
            }
        });


        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence[] items = {"Delete"};
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);


                dialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            // delete
                            Cursor c = MainActivity.dbAdapter.getData("SELECT id FROM NOTES");
                            ArrayList<Integer> arrID = new ArrayList<Integer>();
                            while (c.moveToNext()) {
                                arrID.add(c.getInt(0));
                            }
                            deleteNotes(arrID.get(position));

                        }

                    }
                });
                dialog.show();
                return true;
            }
        });

    }


    public void deleteNotes(final int noteID) {
        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(context);

        dialogDelete.setTitle("Delete a Note");
        dialogDelete.setMessage("Are you sure you want to this delete?");
        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    MainActivity.dbAdapter.deleteData(noteID);
                    Toast.makeText(context, "Delete successfully!!!", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("error", e.getMessage());
                }
                updateNoteList();
            }
        });

        dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }

    private void updateNoteList() {

        Cursor cursor = MainActivity.dbAdapter.getData("SELECT * FROM NOTES");
        MainActivity.list.clear();
        while (cursor.moveToNext()) {

            Integer uid = cursor.getInt(0);
            String utitle = cursor.getString(1);
            String ucontent = cursor.getString(2);
            byte[] uimage = cursor.getBlob(3);

            MainActivity.list.add(new Note(uid, utitle, ucontent, uimage));
        }
        MainActivity.adapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView Title;
        TextView Content;
        ImageView imageView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            Title = (TextView) itemView.findViewById(R.id.txtListTitle);
            Content = (TextView) itemView.findViewById(R.id.txtListContent);
            imageView = (ImageView) itemView.findViewById(R.id.imgListImage);
            cardView = (CardView) itemView.findViewById(R.id.cardview_ID);
        }
    }
}
