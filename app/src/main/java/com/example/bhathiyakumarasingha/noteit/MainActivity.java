package com.example.bhathiyakumarasingha.noteit;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    RecyclerView recyclerView;
    public static ArrayList<Note> list;
    public static NoteAdapter adapter = null;
    public static DBAdapter dbAdapter;
    public SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = (RecyclerView) findViewById(R.id.view_recycler);
        list = new ArrayList<>();
        dbAdapter = new DBAdapter(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.search_action);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_main_add_note:
                Intent newActivity = new Intent(this, AddNoteActivity.class);
                startActivity(newActivity);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    public void getString(String data) {
        System.out.println(data);
    }

    @Override
    protected void onResume() {

        super.onResume();
        int id;
        String title;
        String content;
        byte[] image;

        adapter = new NoteAdapter(this, list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
        System.out.println();
        Cursor cursor = dbAdapter.getData("SELECT * FROM NOTES");
        list.clear();
        while (cursor.moveToNext()) {

            id = cursor.getInt(0);
            title = cursor.getString(1);
            content = cursor.getString(2);
            image = cursor.getBlob(3);

            list.add(new Note(id, title, content, image));
        }
        adapter.notifyDataSetChanged();


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        String sql = "'%" + query + "%'";
        dbAdapter.searchData(sql);
        Cursor cursor = dbAdapter.searchData(sql);
        list.clear();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            String title = cursor.getString(1);
            String content = cursor.getString(2);
            byte[] image = cursor.getBlob(3);

            list.add(new Note(id, title, content, image));
        }
        adapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        String sql = "'%" + newText + "%'";
        dbAdapter.searchData(sql);
        Cursor cursor = dbAdapter.searchData(sql);
        list.clear();
        while (cursor.moveToNext()) {

            int id = cursor.getInt(0);
            String title = cursor.getString(1);
            String content = cursor.getString(2);
            byte[] image = cursor.getBlob(3);

            list.add(new Note(id, title, content, image));
        }
        adapter.notifyDataSetChanged();
        return false;
    }
}
