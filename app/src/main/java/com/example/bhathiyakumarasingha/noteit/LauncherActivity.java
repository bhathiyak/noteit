package com.example.bhathiyakumarasingha.noteit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        Thread splash_Screen = new Thread(){
            public void run(){
                try{
                    sleep(1500);
                }catch(Exception e){
                    e.printStackTrace();
                }finally{
                    startActivity(new
                            Intent(getApplicationContext(),MainActivity.class));
                    finish();
                }
            }
        };
        splash_Screen.start();

    }
}
